namespace WeeklyFuelPricingService.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Series",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SeriesId = c.String(),
                        Name = c.String(),
                        Units = c.String(),
                        F = c.String(),
                        UnitsShort = c.String(),
                        Description = c.String(),
                        Copyright = c.String(),
                        Source = c.String(),
                        Iso3166 = c.String(),
                        Geography = c.String(),
                        Start = c.String(),
                        End = c.String(),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SeriesEntries",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Date = c.String(),
                        Price = c.Double(nullable: false),
                        SeriesId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Series", t => t.SeriesId, cascadeDelete: true)
                .Index(t => t.SeriesId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SeriesEntries", "SeriesId", "dbo.Series");
            DropIndex("dbo.SeriesEntries", new[] { "SeriesId" });
            DropTable("dbo.SeriesEntries");
            DropTable("dbo.Series");
        }
    }
}
