﻿using System.Data.Entity;
using WeeklyFuelPricingService.Models;

namespace WeeklyFuelPricingService.Data
{
    public class FuelPricingContext : DbContext
    {
        public DbSet<Series> Series { get; set; }

        public DbSet<SeriesEntry> SeriesEntries { get; set; }

        public FuelPricingContext() : base("name=FranksSuperFuelServiceDb")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Series>()
                .HasMany(g => g.SeriesEntries)
                .WithRequired(s => s.Series)
                .HasForeignKey(s => s.SeriesId);
        }
    }
}
