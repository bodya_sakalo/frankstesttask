﻿using System;
using System.Data.Entity.Migrations;
using WeeklyFuelPricingService.Scheduler;

namespace WeeklyFuelPricingService
{
    class Program
    {
        private static readonly FuelServiceJobScheduler _fuelServiceJobScheduler = new FuelServiceJobScheduler();

        static void Main(string[] args)
        {
            Console.WriteLine("Ensuring that migrations applied on DB!");

            if (!CheckMigrationsApplied())
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went wrong with migrations if you see this!");
                Console.ForegroundColor = ConsoleColor.Gray;

                Console.WriteLine("Type any key to exit!");
                Console.ReadKey();

                Environment.Exit(0);
                return;
            }

            Console.WriteLine("Everything seems to be fine with migrations!");

            Console.WriteLine("Welcome to Frank`s super fuel pricing service!");

            Console.WriteLine("Background work is about to begin!");

            _fuelServiceJobScheduler.Run();
            
            Console.ReadKey();

            Environment.Exit(0);
        }

        private static bool CheckMigrationsApplied()
        {
            try
            {
                var configuration = new Data.Migrations.Configuration();

                var migrator = new DbMigrator(configuration);

                migrator.Update();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
