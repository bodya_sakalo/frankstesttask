﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using WeeklyFuelPricingService.Data;
using WeeklyFuelPricingService.Models;
using WeeklyFuelPricingService.Services.Abstractions;

namespace WeeklyFuelPricingService.Services
{
    public class SeriesEntryService : IService<SeriesEntry>
    {
        public SeriesEntry GetById(long id)
        {
            using (var db = new FuelPricingContext())
            {
                return db.SeriesEntries.FirstOrDefault(item => item.Id == id);
            }
        }

        public SeriesEntry GetOneByExpression(Expression<Func<SeriesEntry, bool>> expression)
        {
            using (var db = new FuelPricingContext())
            {
                return db.SeriesEntries.FirstOrDefault(expression);
            }
        }

        public IEnumerable<SeriesEntry> GetByExpression(Expression<Func<SeriesEntry, bool>> expression)
        {
            using (var db = new FuelPricingContext())
            {
                return db.SeriesEntries.Where(expression).ToList();
            }
        }

        public void CreateOrUpdate(SeriesEntry seriesEntry)
        {
            using (var db = new FuelPricingContext())
            {
                db.SeriesEntries.AddOrUpdate(seriesEntry);

                db.SaveChanges();
            }
        }
    }
}
