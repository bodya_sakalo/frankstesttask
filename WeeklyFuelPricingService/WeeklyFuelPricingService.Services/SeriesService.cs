﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using WeeklyFuelPricingService.Data;
using WeeklyFuelPricingService.Models;
using WeeklyFuelPricingService.Services.Abstractions;

namespace WeeklyFuelPricingService.Services
{
    public class SeriesService : IService<Series>
    {
        public Series GetById(long id)
        {
            using (var db = new FuelPricingContext())
            {
                return db.Series.FirstOrDefault(item => item.Id == id);
            }
        }

        public Series GetOneByExpression(Expression<Func<Series, bool>> expression)
        {
            using (var db = new FuelPricingContext())
            {
                return db.Series.FirstOrDefault(expression);
            }
        }

        public IEnumerable<Series> GetByExpression(Expression<Func<Series, bool>> expression)
        {
            using (var db = new FuelPricingContext())
            {
                return db.Series.Where(expression).ToList();
            }
        }

        public void CreateOrUpdate(Series series)
        {
            using (var db = new FuelPricingContext())
            {
                db.Series.AddOrUpdate(series);

                db.SaveChanges();
            }
        }
    }
}
