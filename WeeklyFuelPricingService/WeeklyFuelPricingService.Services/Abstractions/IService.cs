﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace WeeklyFuelPricingService.Services.Abstractions
{
    public interface IService<T>
    {
        T GetById(long id);

        T GetOneByExpression(Expression<Func<T, bool>> expression);

        IEnumerable<T> GetByExpression(Expression<Func<T, bool>> expression);

        void CreateOrUpdate(T entity);
    }
}
