﻿using System;

namespace WeeklyFuelPricingService.Dto
{
    public class SeriesEntryDto
    {
        public DateTime Date { get; set; }

        public double Price { get; set; }
    }
}
