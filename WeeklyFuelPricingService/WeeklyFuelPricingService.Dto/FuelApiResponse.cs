﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WeeklyFuelPricingService.Dto
{
    public class FuelApiResponse
    {
        [JsonProperty("series")]
        public IEnumerable<SeriesDto> Series { get; set; }
    }
}
