﻿using Quartz;
using Quartz.Impl;
using System;
using System.Configuration;
using WeeklyFuelPricingService.Models;
using WeeklyFuelPricingService.Scheduler.Jobs;
using WeeklyFuelPricingService.Services;
using WeeklyFuelPricingService.Services.Abstractions;

namespace WeeklyFuelPricingService.Scheduler
{
    public class FuelServiceJobScheduler
    {
        private readonly IService<Series> _seriesService = new SeriesService();
        private readonly IService<SeriesEntry> _seriesEntryService = new SeriesEntryService();

        public void Run()
        {
            try
            {
                // Grab the Scheduler instance from the Factory 
                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
                
                scheduler.Start();

                // create job
                IJobDetail job = JobBuilder.Create<ParseFuelPricingDataJob>()
                        .WithIdentity("job1", "group1")
                        .Build();

                job.JobDataMap["seriesService"] = _seriesService;
                job.JobDataMap["seriesEntryService"] = _seriesEntryService;

                var jobInterval = int.Parse(ConfigurationManager.AppSettings["TaskExecutionDelay"]);

                // create trigger
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger1", "group1")
                    .WithSimpleSchedule(x => x.WithIntervalInSeconds(jobInterval).RepeatForever())
                    .Build();
                
                // Schedule the job using the job and trigger
                scheduler.ScheduleJob(job, trigger);
            }
            catch (SchedulerException se)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(se);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }
}
