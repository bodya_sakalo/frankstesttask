﻿using Newtonsoft.Json;
using Quartz;
using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using WeeklyFuelPricingService.Dto;
using WeeklyFuelPricingService.Models;
using WeeklyFuelPricingService.Services.Abstractions;

namespace WeeklyFuelPricingService.Scheduler.Jobs
{
    public class ParseFuelPricingDataJob : IJob
    {
        public async void Execute(IJobExecutionContext context)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Background work is up and running!");
            Console.ForegroundColor = ConsoleColor.Gray;

            JobDataMap dataMap = context.JobDetail.JobDataMap;

            IService<Series> seriesService = (IService<Series>)dataMap.Get("seriesService");
            IService<SeriesEntry> seriesEntryService = (IService<SeriesEntry>)dataMap.Get("seriesEntryService");

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync("http://api.eia.gov/series/?api_key=ec92aacd6947350dcb894062a4ad2d08&series_id=PET.EMD_EPD2D_PTE_NUS_DPG.W");

                if (response.IsSuccessStatusCode)
                {
                    var stringResponse = await response.Content.ReadAsStringAsync();

                    var fuelApiResponse = JsonConvert.DeserializeObject<FuelApiResponse>(stringResponse);

                    if (fuelApiResponse != null && fuelApiResponse.Series.Any())
                    {
                        var seriesDto = fuelApiResponse.Series.ToList()[0];

                        var seriesBeingProcessed = seriesService
                            .GetOneByExpression(item => item.SeriesId.ToLower().Equals(seriesDto.SeriesId.ToLower()));

                        if (seriesBeingProcessed == null)
                        {
                            seriesBeingProcessed = new Series
                            {
                                Copyright = seriesDto.Copyright,
                                Description = seriesDto.Description,
                                End = seriesDto.End,
                                F = seriesDto.F,
                                Geography = seriesDto.Geography,
                                Iso3166 = seriesDto.Iso3166,
                                Name = seriesDto.Name,
                                SeriesId = seriesDto.SeriesId,
                                Source = seriesDto.Source,
                                Start = seriesDto.Start,
                                Units = seriesDto.Units,
                                UnitsShort = seriesDto.UnitsShort,
                                Updated = seriesDto.Updated
                            };
                        }
                        else
                        {
                            seriesBeingProcessed.Copyright = seriesDto.Copyright;
                            seriesBeingProcessed.Description = seriesDto.Description;
                            seriesBeingProcessed.End = seriesDto.End;
                            seriesBeingProcessed.F = seriesDto.F;
                            seriesBeingProcessed.Geography = seriesDto.Geography;
                            seriesBeingProcessed.Iso3166 = seriesDto.Iso3166;
                            seriesBeingProcessed.Name = seriesDto.Name;
                            seriesBeingProcessed.SeriesId = seriesDto.SeriesId;
                            seriesBeingProcessed.Source = seriesDto.Source;
                            seriesBeingProcessed.Start = seriesDto.Start;
                            seriesBeingProcessed.Units = seriesDto.Units;
                            seriesBeingProcessed.UnitsShort = seriesDto.UnitsShort;
                            seriesBeingProcessed.Updated = seriesDto.Updated;
                        }

                        seriesService.CreateOrUpdate(seriesBeingProcessed);



                        var daysToTake = int.Parse(ConfigurationManager.AppSettings["DaysCount"]);

                        var pastDateForFilter = DateTime.Now.AddDays(-daysToTake);

                        var seriesEntriesFromApi = seriesDto.Data
                            .Select(item => new SeriesEntry
                            {
                                Date = item[0].ToObject<string>(),
                                Price = item.ToArray()[1].ToObject<double>()
                            })
                            .Where(item => DateTime.ParseExact(item.Date, "yyyyMMdd", CultureInfo.InvariantCulture) >= pastDateForFilter)
                            .OrderByDescending(item => item.Date)
                            .ToList();

                        var seriesEntriesFromFromDb = seriesEntryService.GetByExpression(item => item.SeriesId == seriesBeingProcessed.Id);

                        var newSeriesEntries = seriesEntriesFromApi.Where(item => !seriesEntriesFromFromDb.Any(e => e.Date.Equals(item.Date)));

                        newSeriesEntries.ToList().ForEach(item =>
                        {
                            item.SeriesId = seriesBeingProcessed.Id;

                            seriesEntryService.CreateOrUpdate(item);
                        });
                    }
                }
            }

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Background work finished!");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("Type any key if you want to exit!");
        }
    }
}
