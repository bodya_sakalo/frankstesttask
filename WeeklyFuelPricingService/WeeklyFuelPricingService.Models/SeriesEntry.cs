﻿using WeeklyFuelPricingService.Models.Base;

namespace WeeklyFuelPricingService.Models
{
    public class SeriesEntry : BaseModel
    {
        public string Date { get; set; }

        public double Price { get; set; }


        public long SeriesId { get; set; }
        public virtual Series Series { get; set; }
    }
}
