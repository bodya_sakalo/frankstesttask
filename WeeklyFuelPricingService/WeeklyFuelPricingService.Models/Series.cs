﻿using System;
using System.Collections.Generic;
using WeeklyFuelPricingService.Models.Base;

namespace WeeklyFuelPricingService.Models
{
    public class Series : BaseModel
    {
        public string SeriesId { get; set; }
        
        public string Name { get; set; }
        
        public string Units { get; set; }
        
        public string F { get; set; }
        
        public string UnitsShort { get; set; }
        
        public string Description { get; set; }
        
        public string Copyright { get; set; }
        
        public string Source { get; set; }
        
        public string Iso3166 { get; set; }
        
        public string Geography { get; set; }
        
        public string Start { get; set; }
        
        public string End { get; set; }
        
        public DateTime Updated { get; set; }


        public virtual ICollection<SeriesEntry> SeriesEntries { get; set; }
    }
}
