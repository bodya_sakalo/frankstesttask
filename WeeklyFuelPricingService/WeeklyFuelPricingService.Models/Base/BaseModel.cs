﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WeeklyFuelPricingService.Models.Base
{
    public class BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
    }
}
